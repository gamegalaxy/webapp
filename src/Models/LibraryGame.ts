import { Game } from "./Game";
import { Platform } from "./Platform";
import { User } from "./User";

export class LibraryGame {
    id?: number;
    game?: Game;
    user?: User;
    platforms?: Platform[];
}