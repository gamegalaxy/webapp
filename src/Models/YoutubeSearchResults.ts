export class YoutubeSearchResults {
    kind?: string;
    etag?: string;
    nextPageToken?: string;
    regionCode?: string;
    pageInfo?: {
        totalResults: number,
        resultsPerPage: number
    };
    items?: YoutubeSearchResult[];
}

export class YoutubeSearchResult {
    kind?: string;
    etag?: string;
    id: {
        kind?: string,
        videoId: string
    } = { videoId: '' };
    snippet?: {
        publishedAt?: Date,
        channelId?: string,
        title?: string,
        description?: string,
        thumbnails?: {
            default?: {
                url?: string,
                width?: string,
                height?: string
            },
            medium?: {
                url?: string,
                width?: string,
                height?: string
            },
            high?: {
                url?: string,
                width?: string,
                height?: string
            }
        },
        channelTitle?: string,
        liveBroadcastContent?: string,
        publishTime?: Date
    }
}