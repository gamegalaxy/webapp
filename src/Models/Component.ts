import ComponentBrand from "../Enum/componentBrand";
import ComponentType from "../Enum/componentType";

export class Component {
    id?: number;
    type?: ComponentType;
    brand?: ComponentBrand;
    model?: string;
    benchmark?: number;
}