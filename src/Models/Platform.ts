export class Platform {
    id?: number;
    name?: string;
    isPC?: boolean;
    icon?: string;
}