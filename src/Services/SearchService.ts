import Axios from "axios";
import { Game } from "../Models/Game";

class SearchService {

    url: string = "https://api.gamegalaxy.appyond.com/search/game";

    searchGame(term: string): Promise<Game[]> {
        return Axios.get(this.url + '?term=' + term).then((data) => {
            return data.data as Game[];
        });
    }

}

export default new SearchService();