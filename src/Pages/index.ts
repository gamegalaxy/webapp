import { GamePage } from './GamePage';
import { AdminGamePage } from './AdminGamePage';
import { FormPage } from './newGame';
import { EditGame } from './editGame';
export {
    GamePage,
    AdminGamePage,
    FormPage,
    EditGame
}