import React, { useEffect, useState } from 'react';
import Video1 from '../Components/Video1';
import GameDetails from '../Components/gameDetails';
import {NavigationBar} from '../Components/NavigationBar';
import '../Components/videos.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import './GamePage.scss';
import { Game } from '../Models/Game';
import { YoutubeSearchResult } from '../Models/YoutubeSearchResults';
import GameService from '../Services/GameService';
import MediaService from '../Services/MediaService';
import { useParams } from 'react-router-dom';
import EraseButton from '../Components/eraseButton';
export const AdminGamePage = () => {
    let { id } = useParams() as any;

    const [game, setGame] = useState(new Game());
    useEffect(() => {
        GameService.getById(id).then(g => {
            console.log(g);
            setGame(g);
        });
    }, [id])


    const [video, setYoutubeSearchResults] = useState([] as YoutubeSearchResult[])
    useEffect(() => {
        MediaService.getVideos(id).then(v => {
            console.log(v);
            setYoutubeSearchResults(v.items!.slice(0,3));
        });
    }, [id])

    
    return (
     
        <> 
        <NavigationBar></NavigationBar>
        <EraseButton id={game.id}></EraseButton>
        { 
            game &&
            <div className="App-header">
                <GameDetails game={game}></GameDetails>
                <h3 className="lessmargin">Videos</h3>
                <div className="cointainer row">
                    {
                        video.map(v => {
                            return <Video1 id={v.id.videoId} key={v.id!.videoId}></Video1>
                        })
                    }
                </div>
            </div>
        }
    </>
    );
}