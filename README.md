# GameGalaxy

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Before running the project, while being in the project directory, you have to run the next command at the terminal:

### `yarn install`

This command will install all the necessary modules in the project directory.
After that, for starting it, you have to run:

### `yarn start`

This command uns the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


###### `yarn test`
This command launches the test runner in the interactive watch mode.\


###### `yarn build`
This command builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.


### The app is ready to be deployed!



**Puntos extras realizados:**
- UnitTesting 5 (Se puede ver en GitLab )
- Automatic test & deployment (CI/CD) 5 (Se puede ver en GitLab)
- UX design 5 (En [Google drive](https://drive.google.com/drive/folders/1FoFGMJapfF7aHqzgUh3AEGDH9FOMmaaj?usp=sharing))
- Responsiva 5
